import mill._, scalalib._, scalafmt._

object printful extends ScalaModule with ScalafmtModule {
  override def scalaVersion = "2.13.2"

  override def sources = T.sources { millSourcePath / os.up / 'src / 'main / 'scala }
  override def resources = T.sources { millSourcePath / os.up / 'src / 'main / 'resources }

  override def mainClass = Some("com.printful.PrintfulApi")

  override def scalacOptions = T { Seq (
    "-deprecation",                      // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8",                // Specify character encoding used by source files.
    "-explaintypes",                     // Explain type errors in more detail.
    "-feature",                          // Emit warning and location for usages of features that should be imported explicitly.
    "-language:existentials",            // Existential types (besides wildcard types) can be written and inferred
    "-language:experimental.macros",     // Allow macro definition (besides implementation and application)
    "-language:higherKinds",             // Allow higher-kinded types
    "-language:implicitConversions",     // Allow definition of implicit functions called views
    "-unchecked",                        // Enable additional warnings where generated code depends on assumptions.
    "-Xcheckinit",                       // Wrap field accessors to throw an exception on uninitialized access.
    //  "-Xfatal-warnings",                  // Fail the compilation if there are any warnings.
    "-Xlint:adapted-args",               // Warn if an argument list is modified to match the receiver.
    "-Xlint:constant",                   // Evaluation of a constant arithmetic expression results in an error.
    "-Xlint:delayedinit-select",         // Selecting member of DelayedInit.
    "-Xlint:doc-detached",               // A Scaladoc comment appears to be detached from its element.
    "-Xlint:inaccessible",               // Warn about inaccessible types in method signatures.
    "-Xlint:infer-any",                  // Warn when a type argument is inferred to be `Any`.
    "-Xlint:missing-interpolator",       // A string literal appears to be missing an interpolator id.
    "-Xlint:nullary-override",           // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Xlint:nullary-unit",               // Warn when nullary methods return Unit.
    "-Xlint:option-implicit",            // Option.apply used implicit view.
    "-Xlint:package-object-classes",     // Class or object defined in package object.
    "-Xlint:poly-implicit-overload",     // Parameterized overloaded implicit methods are not visible as view bounds.
    "-Xlint:private-shadow",             // A private field (or class parameter) shadows a superclass field.
    "-Xlint:stars-align",                // Pattern sequence wildcard must align with sequence component.
    "-Xlint:type-parameter-shadow",      // A local type parameter shadows a type already in scope.
    //  "-Ywarn-dead-code",                  // Warn when dead code is identified.
    "-Ywarn-extra-implicit",             // Warn when more than one implicit parameter section is defined.
    "-Ywarn-numeric-widen",              // Warn when numerics are widened.
    //  "-Ywarn-unused:implicits",           // Warn if an implicit parameter is unused.
    //  "-Ywarn-unused:imports",             // Warn if an import selector is not referenced.
    "-Ywarn-unused:locals",              // Warn if a local definition is unused.
    //  "-Ywarn-unused:params",              // Warn if a value parameter is unused.
    "-Ywarn-unused:patvars",             // Warn if a variable bound in a pattern is unused.
    "-Ywarn-unused:privates",            // Warn if a private member is unused.
    "-Ywarn-value-discard",               // Warn when non-Unit expression results are unused.
    "-Ymacro-annotations"                 // enable simulacrum typeclass annotations only in 2.13
  )}

  object Versions {
    val circe = "0.13.0"
    val circeYaml = "0.13.1"
    val sttp = "2.1.4"
    val akkaHttp = "10.1.11"
    val tapir = "0.14.5"
    val akkaStreams = "2.6.5"
    val swaggerUi = "3.25.1"
    val scalaCheck = "1.14.3"
    val scalaTest = "3.0.8"
    val refined = "0.9.14"
    val enumeratum = "1.6.0"
    val caliban = "0.8.0"
  }

  override def ivyDeps = Agg(
    ivy"org.scalatest::scalatest:3.1.1",
    ivy"com.softwaremill.sttp.tapir::tapir-core:${Versions.tapir}",
    ivy"com.softwaremill.sttp.tapir::tapir-openapi-docs:${Versions.tapir}",
    ivy"com.softwaremill.sttp.tapir::tapir-openapi-circe-yaml:${Versions.tapir}",
    ivy"com.softwaremill.sttp.tapir::tapir-swagger-ui-akka-http:${Versions.tapir}",
    ivy"com.softwaremill.sttp.tapir::tapir-json-circe:${Versions.tapir}",
    ivy"com.softwaremill.sttp.tapir::tapir-akka-http-server:${Versions.tapir}",
    ivy"com.softwaremill.sttp.tapir::tapir-sttp-client:${Versions.tapir}",
    ivy"com.typesafe.akka::akka-http:${Versions.akkaHttp}",
    ivy"com.typesafe.akka::akka-stream:${Versions.akkaStreams}",
    ivy"org.webjars:swagger-ui:${Versions.swaggerUi}",
    ivy"io.circe::circe-core:${Versions.circe}",
    ivy"io.circe::circe-generic:${Versions.circe}",
    ivy"io.circe::circe-parser:${Versions.circe}",
    ivy"ch.qos.logback:logback-classic:1.2.3",
    ivy"ch.qos.logback:logback-core:1.2.3",
    ivy"com.typesafe.scala-logging::scala-logging:3.9.2",
    ivy"com.softwaremill.sttp.client::core:${Versions.sttp}",
    ivy"com.softwaremill.sttp.client::slf4j-backend:${Versions.sttp}",
    ivy"com.softwaremill.sttp.client::async-http-client-backend-zio:${Versions.sttp}",
    ivy"com.github.ghostdogpr::caliban-tapir:${Versions.caliban}",
    ivy"com.github.ghostdogpr::caliban-akka-http:${Versions.caliban}", // routes for akka-http
    ivy"de.heikoseeberger::akka-http-circe:1.32.0", // required by caliban graphql

    //    ivy"com.softwaremill.sttp.client::async-http-client-backend-fs2:${Versions.sttp % "test"
//
//      ivy"org.springframework.boot:spring-boot-starter-webflux:2.1.6.RELEASE",
//      ivy"com.fasterxml.jackson.module::jackson-module-scala:2.9.9",
//
//      ivy"org.typelevel::cats-core:2.0.0-M4",
//      ivy"com.chuusai::shapeless:2.3.3",
//      ivy"com.github.mpilquist::simulacrum:0.19.0",
  )

//  object test extends Tests {
//    def testFrameworks = T { Seq("org.scalatest.tools.Framework") }
//
//    override def sources = T.sources { millSourcePath / os.up / os.up / 'src / 'test / 'scala }
//
//    override def ivyDeps = Agg(
//      ivy"org.scalatest::scalatest:3.0.8"
//    )
//  }
}
