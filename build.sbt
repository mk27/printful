ThisBuild / scalaVersion := "2.13.2"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.printful"
ThisBuild / organizationName := "api"

//lazy val root = (project in file("."))
//  .settings(
name := "printful"
libraryDependencies ++= List(
  "org.scalatest" %% "scalatest" % "3.1.1",
  "com.softwaremill.sttp.tapir" %% "tapir-core" % Versions.tapir,
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % Versions.tapir,
  "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % Versions.tapir,
  "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-akka-http" % Versions.tapir,
  "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % Versions.tapir,
  "com.softwaremill.sttp.tapir" %% "tapir-akka-http-server" % Versions.tapir,
  "com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % Versions.tapir,
  "com.typesafe.akka" %% "akka-http" % Versions.akkaHttp,
  "com.typesafe.akka" %% "akka-stream" % Versions.akkaStreams,
  "org.webjars" % "swagger-ui" % Versions.swaggerUi,
  "io.circe" %% "circe-core" % Versions.circe,
  "io.circe" %% "circe-generic" % Versions.circe,
  "io.circe" %% "circe-parser" % Versions.circe,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "ch.qos.logback" % "logback-core" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "com.softwaremill.sttp.client" %% "core" % Versions.sttp,
  "com.softwaremill.sttp.client" %% "slf4j-backend" % Versions.sttp,
  "com.softwaremill.sttp.client" %% "async-http-client-backend-fs2" % Versions.sttp % "test",
  // graphql
  "com.github.ghostdogpr" %% "caliban-tapir" % "0.8.0",
  "com.github.ghostdogpr" %% "caliban-akka-http"  % "0.8.0", // routes for akka-http
  "com.softwaremill.sttp.client" %% "async-http-client-backend-zio" % Versions.sttp,
  "de.heikoseeberger" %% "akka-http-circe" % "1.32.0",
)
//  )
//addCompilerPlugin("io.tryp" % "splain" % "0.5.5" cross CrossVersion.patch)
//
//scalacOptions += "-P:splain:all:true -Xlog-implicits"
