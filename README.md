This project provides an [OpenApi](https://swagger.io/specification/) specification for [www.printful.com](printful.com) api.  

Api is described using the fantastic [Tapir](https://github.com/softwaremill/tapir) library.

To run the project execute:

```bash
./sbt run
# or mill printful run
```
and open [http://localhost:8082/docs](http://localhost:8082/docs)

For GraphQL wrapper goto [http://localhost:8082/graphiql.html](http://localhost:8082/graphiql.html) 

At this point in time it is not possible to make cross origin requests to the http://api.printful.com
The implementation provides a proxy to walk around the issue.

This Api is in no way complete it provides just a preview with type defitions and describes just 2 endpoints.

A static preview is available at [https://mk27.gitlab.io/printful/](https://mk27.gitlab.io/printful/) purposes the generated docs.yaml are published to gitlab pages. 
