package com.printful.endpoints

import com.printful.PrintfulApi.{GetVariantResponse, PostRequestVariant, PostVariantResponse, PutRequestVariant}
import sttp.tapir.model.UsernamePassword

object Store {
  import io.circe.generic.auto._
  import sttp.tapir._
  import sttp.tapir.json.circe._

  private val storeEndpoint = Base.baseEndpoint.in("store")

  val getVariantEndpoint: Endpoint[(UsernamePassword, Int), String, GetVariantResponse, Nothing] =
    storeEndpoint.get
      .tag("variants")
      .in(auth.basic[UsernamePassword])
      .in("variants" / path[Int]("variantId"))
      .out(jsonBody[GetVariantResponse])

  val postVariantEndpoint: Endpoint[(UsernamePassword, Int, PostRequestVariant), String, PostVariantResponse, Nothing] =
    storeEndpoint.post
      .tag("variants")
      .in(auth.basic[UsernamePassword])
      .in("products" / path[Int]("productId") / "variants")
      .in(jsonBody[PostRequestVariant])
      .out(jsonBody[PostVariantResponse])

  val putVariantEndpoint: Endpoint[(UsernamePassword, Int, PutRequestVariant), String, GetVariantResponse, Nothing] =
    storeEndpoint.put
      .tag("variants")
      .in(auth.basic[UsernamePassword])
      .in("variants" / path[Int]("variantId"))
      .in(jsonBody[PutRequestVariant])
      .out(jsonBody[GetVariantResponse])

}
