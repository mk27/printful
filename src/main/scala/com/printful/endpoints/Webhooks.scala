package com.printful.endpoints

import com.printful.PrintfulApi.{DeleteWebhookResponse, GetWebhookResponse, PostWebhookRequest, PostWebhookResponse}
import sttp.tapir.model.UsernamePassword

object Webhooks {
  import io.circe.generic.auto._
  import sttp.tapir._
  import sttp.tapir.json.circe._

  private val webhookEndpoint = Base.baseEndpoint
    .tag("webhooks")
    .in(auth.basic[UsernamePassword])
    .in("webhooks")

  val postWebhooksEndpoint: Endpoint[(UsernamePassword, PostWebhookRequest), String, PostWebhookResponse, Nothing] =
    webhookEndpoint.post
      .in(jsonBody[PostWebhookRequest])
      .out(jsonBody[PostWebhookResponse])

  val getWebhooksEndpoint: Endpoint[UsernamePassword, String, GetWebhookResponse, Nothing] =
    webhookEndpoint.get
      .out(jsonBody[GetWebhookResponse])

  val deleteWebhooksEndpoints: Endpoint[UsernamePassword, String, DeleteWebhookResponse, Nothing] =
    webhookEndpoint.delete
      .out(jsonBody[DeleteWebhookResponse])

}
