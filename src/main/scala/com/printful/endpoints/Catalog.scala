package com.printful.endpoints

import com.printful.PrintfulApi.{GetProductListResponse, GetProductVariantListResponse, GetProductVariantResponse}
import sttp.tapir.model.UsernamePassword

object Catalog {
  import io.circe.generic.auto._
  import sttp.tapir._
  import sttp.tapir.json.circe._

  private val pc = Base.baseEndpoint.get
    .tag("catalog")
    .in(auth.basic[UsernamePassword])

  val getProductListEndpoint: Endpoint[UsernamePassword, String, GetProductListResponse, Nothing] =
    pc.in("products")
      .name("catalogProducts")
      .out(jsonBody[GetProductListResponse])

  val getProductEndpoint: Endpoint[(UsernamePassword, Int), String, GetProductVariantListResponse, Nothing] =
    pc.in("products" / path[Int]("productId").example(5))
      .name("catalogProductById")
      .out(jsonBody[GetProductVariantListResponse])

  val getProductVariantEndpoint: Endpoint[(UsernamePassword, Int), String, GetProductVariantResponse, Nothing] =
    pc.in("products" / "variant" / path[Int]("variantId").example(6834))
      .name("catalogVariantById")
      .out(jsonBody[GetProductVariantResponse])

}
