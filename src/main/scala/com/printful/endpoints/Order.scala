package com.printful.endpoints

import com.printful.PrintfulApi.{Costs, OrderListResponse, OrderResponse}
import sttp.tapir.EndpointIO.Example
import sttp.tapir.model.UsernamePassword

object order {
  import io.circe.generic.auto._
  import sttp.tapir._
  import sttp.tapir.json.circe._

  case class OrderInput(
    external_id: String,
    shipping: String,
    recipient: Address,
    items: List[Item]
//    gift: Option[GiftData],
//    packing_slip: Option[PackingSlip]
    //    confirm: Boolean,
    //    update_existing: Boolean
  )

  object OrderInput {

    val example: Example[OrderInput] =
      Example.of(OrderInput("xyz", "STANDARD", Address.example, List(Item.example)))
  }
  case class Address(
    name: String,
//    company: Option[String],
    address1: String,
    address2: Option[String],
    city: String,
    state_code: Option[String] = None,
    state_name: Option[String] = None,
    country_code: String,
    country_name: String,
    zip: String,
    phone: Option[String],
    email: Option[String]
  )

  object Address {

    val example = Address(
      "jan smith",
      "417 MONTGOMERY ST STE 500",
      None,
      "SAN FRANCISCO",
      Some("CA"),
      Some("California"),
      "US",
      "US",
      "94104-1100",
      None,
      Some("my@email.com")
    )
  }
  case class Item(
//    external_id: String
    external_variant_id: String,
    quantity: Int
  )

  object Item {

    val example = Item(
      "1582342deefc40518b23dc83125ec2a6",
      1
    )
  }
  case class GiftData()
  case class PackingSlip()
  case class PostOrderResponse(
    code: Int,
    result: Order
  )
  case class Order(
    external_id: String,
    status: String,
    shipping: String
  )

  case class EstimateResponse(code: Int, result: OrderCosts)
  case class OrderCosts(
    costs: Costs
//    retail_costs: Costs
  )

  private val orders =
    Base.baseEndpoint
      .tag("orders")
      .in(auth.basic[UsernamePassword])

  val estimateOrderEndpoint: Endpoint[(UsernamePassword, OrderInput), String, EstimateResponse, Nothing] =
    orders.post
      .in("orders" / "estimate-costs")
      .in(jsonBody[OrderInput].example(OrderInput.example))
      .out(jsonBody[EstimateResponse])

  val postOrderEndpoint
    : Endpoint[(UsernamePassword, Option[Boolean], Option[Boolean], OrderInput), String, PostOrderResponse, Nothing] =
    orders.post
      .in("orders")
      .in(query[Option[Boolean]]("commit"))
      .in(query[Option[Boolean]]("update_existing"))
      .in(jsonBody[OrderInput].example(OrderInput.example))
      .out(jsonBody[PostOrderResponse])

  val getOrderEndpoint: Endpoint[(UsernamePassword, String), String, OrderResponse, Nothing] =
    orders.get
      .name("getOrder")
      .description("getOrder")
      .in("orders" / path[String]("orderId"))
      .out(jsonBody[OrderResponse])

  val getOrderListEndpoint: Endpoint[UsernamePassword, String, OrderListResponse, Nothing] =
    orders.get
      .in("orders")
      .description("getOrderList")
      .name("getOrderList")
      .out(jsonBody[OrderListResponse])
}
