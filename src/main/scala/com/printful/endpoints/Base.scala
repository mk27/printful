package com.printful.endpoints

import sttp.tapir.{endpoint, stringBody, Endpoint}

object Base {
  val baseEndpoint: Endpoint[Unit, String, Unit, Nothing] = endpoint.errorOut(stringBody)
}
