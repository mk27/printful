package com.printful.endpoints

import com.printful.PrintfulApi.{
  GetVariantResponse,
  PostProductResponse,
  PostRequestVariant,
  PostVariantResponse,
  ProductResponse,
  ProductsResponse,
  PutRequestVariant,
  RequestProductBody
}
import sttp.tapir.model.UsernamePassword

object Products {
  import io.circe.generic.auto._
  import sttp.tapir._
  import sttp.tapir.json.circe._

  private val storeEndpoint = Base.baseEndpoint.in("store")

  private val products =
    storeEndpoint
      .tag("products")
      .in(auth.basic[UsernamePassword])

  val getProductsList: Endpoint[UsernamePassword, String, ProductsResponse, Nothing] =
    products.get
      .in("products")
      .name("storeProducts")
      .out(jsonBody[ProductsResponse])

  val getProductEndpoint: Endpoint[(UsernamePassword, Int), String, ProductResponse, Nothing] =
    products.get
      .name("storeProductsById")
      .in("products" / path[Int]("productId"))
      .out(jsonBody[ProductResponse])

  val postProductEndpoint: Endpoint[(UsernamePassword, RequestProductBody), String, PostProductResponse, Nothing] =
    products.post
      .in("products")
      .in(jsonBody[RequestProductBody])
      .out(jsonBody[PostProductResponse])

  private val variant = storeEndpoint
    .tag("variants")
    .in(auth.basic[UsernamePassword])

  val getVariantEndpoint: Endpoint[(UsernamePassword, Int), String, GetVariantResponse, Nothing] =
    variant.get
      .name("storeVariantById")
      .in("variants" / path[Int]("variantId"))
      .out(jsonBody[GetVariantResponse])

  val postVariantEndpoint: Endpoint[(UsernamePassword, Int, PostRequestVariant), String, PostVariantResponse, Nothing] =
    variant.post
      .in("products" / path[Int]("productId") / "variants")
      .in(jsonBody[PostRequestVariant])
      .out(jsonBody[PostVariantResponse])

  val putVariantEndpoint: Endpoint[(UsernamePassword, Int, PutRequestVariant), String, GetVariantResponse, Nothing] =
    variant.put
      .in("variants" / path[Int]("variantId"))
      .in(jsonBody[PutRequestVariant])
      .out(jsonBody[GetVariantResponse])
}
