package com.printful

import com.printful.endpoints.{order, Base, Catalog}
import com.typesafe.scalalogging.StrictLogging
import sttp.model.Uri
import sttp.tapir.Endpoint
import sttp.tapir.model.UsernamePassword
import sttp.tapir.openapi.Server
import sttp.tapir.swagger.akkahttp.SwaggerAkka

object PrintfulApi extends App with StrictLogging {
  type Limit = Option[Int]
  val port = 8082

  case class SyncProduct(id: Int, external_id: String, name: String, variants: Int, synced: Int, thumbnail_url: String)

  case class SyncProductInfo(sync_product: SyncProduct, sync_variants: List[SyncVariant])
  case class ProductVariant(variant_id: Int, product_id: Int, image: String, name: String)

  case class File(
    id: Int,
    `type`: String,
    hash: String,
    url: Option[String],
    filename: String,
    mime_type: String,
    size: Int,
    width: Int,
    height: Int,
    dpi: Option[Int],
    status: String,
    created: Long,
    thumbnail_url: String,
    preview_url: String,
    visible: Boolean
  )

  case class ItemOption(
    id: String
//                        , value: Either[String, List[String]]
  )

  object ItemOption {
//    val codec = Codec.
//    implicit val decoder: Decode[ItemOption, Json] = new Decode[ItemOption, Json] {
//      override def rawDecode(s: Json): DecodeResult[ItemOption] =
//        s.
//    }
  }
  case class SyncVariant(
    id: Int,
    external_id: String,
    sync_product_id: Int,
    name: String,
    synced: Boolean,
    variant_id: Int,
    retail_price: BigDecimal,
    currency: String,
    product: ProductVariant,
    files: List[File],
    options: List[ItemOption]
  )

  case class Page(total: Int, offset: Int, limit: Int)
  case class ProductsResponse(code: Int, result: List[SyncProduct], paging: Page)
  case class ProductResponse(code: Int, result: SyncProductInfo)

  case class PutRequestProductBody(sync_product: PutRequestProduct, sync_variants: List[PutRequestVariant])
  case class PutRequestProduct(external_id: String, name: String, thumbnail: String)

  case class RequestFile(`type`: String, id: Option[Int], url: Option[String]) {
    require(id.isDefined ^ url.isDefined, "Provide either id or url")
  }

  case class PutRequestVariant(
    id: String,
    external_id: String,
    variant_id: Int,
    retail_price: Double,
    files: List[RequestFile],
    options: List[ItemOption])
  type PostRequestVariant = PutRequestVariant

  case class PutProductResponse(code: Int, result: RequestProductResponse)

  case class PostVariantResponse(
    code: Int,
    result: RequestVariantResponse
  )

  case class RequestVariantResponse(
    id: Int,
    external_id: String,
    sync_product_id: Int,
    name: String,
    synced: Boolean,
    variant_id: Int,
    retail_price: BigDecimal,
    currency: String,
    product: ProductMiniInfo,
    files: List[File],
    options: List[ItemOption]
  )

  case class RequestProductResponse(id: Int, external_id: String, name: String, variants: Int, synced: Int)

  case class SyncVariantInfo(
    sync_variant: SyncVariant,
    sync_product: SyncProduct
  )

  case class ProductMiniInfo(
    variant_id: Int,
    product_id: Int,
    image: String,
    name: String
  )

//  case class RequestVariantResponse(
//    id: Int,
//    external_id: String,
//    sync_product_id: Int,
//    name: String,
//    synced: Boolean,
//    variant_id: Int,
//    retail_price: Float,
//    currency: String,
//    product: ProductMiniInfo,
//    files: List[File],
//    options: List[ItemOption]
//  )

  case class GetVariantResponse(code: Int, result: SyncVariant)

  case class PostWebhookRequest(url: String, types: List[String])
  case class WebhookInfo(url: Option[String], types: List[String])
  case class GetWebhookResponse(code: Int, result: WebhookInfo)
  case class PostWebhookResponse(code: Int, result: WebhookInfo)
  case class DeleteWebhookResponse(code: Int, result: WebhookInfo)

  case class TemplateVariantMappingItem(placement: String, template_id: Int)
  case class TemplateVariantMapping(
    variant_id: Int,
    templates: List[TemplateVariantMappingItem]
  )
  case class Template(
    template_id: Int,
    image_url: String,
    background_url: Option[String],
    background_color: String,
    printfile_id: Int,
    template_width: Int,
    template_height: Int,
    print_area_width: Int,
    print_area_height: Int,
    print_area_top: Int,
    print_area_left: Int,
    is_template_on_front: Boolean,
    orientation: String
  )
  case class ProductTemplates(
    version: Int,
    min_dpi: Int,
    variant_mapping: List[TemplateVariantMapping],
    templates: List[Template]
  )
  case class GetLayoutResponse(code: Int, result: ProductTemplates)

  case class GenerationTaskExtraMockup(
    title: String,
    url: String,
    option: String,
    option_group: String
  )
  case class GenerationTaskMockups(
    placement: String,
    variant_ids: List[Int],
    mockup_url: String,
    extra: List[GenerationTaskExtraMockup]
  )
  case class GenerationTask(
    task_key: String,
    status: String,
    error: Option[String],
    mockups: List[GenerationTaskMockups]
  )
  case class GetTaskResponse(code: Int, result: GenerationTask)

  case class RequestProduct(
    external_id: String,
    name: String,
    thumbnail: String
  )
  case class RequestVariant(
    external_id: String,
    variant_id: Int,
    retail_price: Float,
    files: List[RequestFile],
    options: List[ItemOption]
  )
  case class RequestProductBody(
    sync_product: RequestProduct,
    sync_variants: List[RequestVariant]
  )
  type PostProductResponse = PutProductResponse

  case class GetProductListResponse(code: Int, result: List[PProduct])
  case class PProduct(
    id: Int,
    `type`: String,
    `type_name`: String,
    brand: Option[String],
    model: String,
    image: Option[String],
    variant_count: Int,
    currency: String,
    files: List[FileType],
    options: List[OptionType],
    is_discontinued: Boolean,
    avg_fulfillment_time: Float,
    description: String
  )
  case class FileType(
    id: String,
    `type`: String,
    title: String,
    additional_price: Option[String]
  )
  case class OptionType(
    id: String,
    title: String,
    `type`: String,
//    values: Option[Map[String, String]],
    additional_price: Option[String]
//    additional_price_breakdown: Map[String, String]
  )

  case class GetProductVariantListResponse(
    code: Int,
    result: ProductInfo
  )
  case class ProductInfo(product: PProduct, variants: List[Variant])
  case class Variant(
    id: Int,
    product_id: Int,
    name: String,
    size: String,
    color: Option[String],
    color_code: Option[String],
    color_code2: Option[String],
    image: String,
    price: String,
    in_stock: Boolean,
    availability_regions: Map[String, String]
  )

  case class GetProductVariantResponse(code: Int, result: VariantInfo)
  case class VariantInfo(product: PProduct, variant: Variant)
  //  object MyCodec {
//    import io.circe._, io.circe.generic.semiauto._
//
//    implicit val PostProductResponseDecoder: Decoder[PostProductResponse] = deriveDecoder[PostProductResponse]
//    implicit val PostProductResponseEncoder: Encoder[PostProductResponse] = deriveEncoder[PostProductResponse]
//
//  }

  case class ShippingRequest(
    recipient: AddressInfo,
    items: List[ItemInfo],
    currency: String,
    locale: Option[String]
  )

  object ShippingRequest {

    val example = ShippingRequest(
      recipient = AddressInfo(
        "Kochanowskiego 4/9",
        "Poznań",
        "PL",
        "",
        "60844"
      ),
      items = List(ItemInfo(Some("1582342deefc40518b23dc83125ec2a6"), 1)),
      currency = "USD",
      locale = None
    )
  }
  case class ItemInfo(
    external_variant_id: Option[String],
    quantity: Int
  )
  case class AddressInfo(
    address1: String,
    city: String,
    country_code: String,
    state_code: String,
    zip: String
  )
  case class ShippingResponse(
    code: Int,
    result: List[ShippingInfo]
  )
  case class ShippingInfo(
    id: String,
    name: String,
    rate: String,
    currency: String,
    minDeliveryDays: Int,
    maxDeliveryDays: Int
  )

  case class TaxRequest(recipient: TaxAddress)
  case class TaxAddress(country_code: String, state_code: String, city: String, zip: String)

  object TaxAddress {

    val example = TaxAddress(
      "US",
      "CA",
      "San Francisco",
      "94104-1100"
    )
  }
  case class TaxResponse(code: Int, result: TaxInfo)

  /**
    * @param required Whether sales tax is required for the given address
    * @param rate     Tax rate
    */
  case class TaxInfo(required: Boolean, rate: Float)

  case class OrderListResponse(
    code: Int,
    result: List[Order]
  )
  case class OrderResponse(
    code: Int,
    result: Order
  )
  case class Order(
    id: Int,
    external_id: Option[String],
    store: Int,
    status: String,
    shipping: String,
    created: Long, // timestamp ?
    updated: Long,
    items: List[ItemInfo],
    recipient: order.Address,
    //    shipments:
    costs: Costs
  )
  case class Costs(
    currency: String,
    subtotal: BigDecimal,
    discount: BigDecimal,
    shipping: BigDecimal,
    digitization: BigDecimal,
    additional_fee: BigDecimal,
    fulfillment_fee: BigDecimal,
    tax: BigDecimal,
    vat: BigDecimal,
    total: BigDecimal
  )

  /**
    * Descriptions of endpoints used in the example.
    */
  object Endpoints {
    import io.circe.generic.auto._
    import sttp.tapir._
    import sttp.tapir.json.circe._
//    import MyCodec._

    // All endpoints report errors as strings, and have the common path prefix '/books'
    val baseEndpoint = Base.baseEndpoint

    val mockupsEndpoint = baseEndpoint
      .tag("mockup-generator")
      .in(auth.basic[UsernamePassword])
      .in("mockup-generator")

    val getTaskEndpoint: Endpoint[(UsernamePassword, String), String, GetTaskResponse, Nothing] =
      mockupsEndpoint
        .in("task")
        .in(query[String]("task_key"))
        .out(jsonBody[GetTaskResponse])

    val getLayoutEndpoint: Endpoint[(UsernamePassword, Int), String, GetLayoutResponse, Nothing] =
      mockupsEndpoint
        .in("templates" / path[Int]("productId").example(5))
        .out(jsonBody[GetLayoutResponse])

    val getShippingRatesEndpoint: Endpoint[(UsernamePassword, ShippingRequest), String, ShippingResponse, Nothing] =
      baseEndpoint.post
        .tag("shipping")
        .in(auth.basic[UsernamePassword])
        .in("shipping" / "rates")
        .in(jsonBody[ShippingRequest].example(ShippingRequest.example))
        .out(jsonBody[ShippingResponse])

    val getTaxEndpoint: Endpoint[TaxRequest, String, TaxResponse, Nothing] = baseEndpoint.post
      .tag("tax")
      .in("tax" / "rates")
      .in(jsonBody[TaxRequest].example(TaxRequest(TaxAddress.example)))
      .out(jsonBody[TaxResponse])
  }

  import Endpoints._
  import akka.http.scaladsl.server.Route
  import endpoints.{Products, Webhooks}

  def openapiYamlDocumentation: String = {
    import sttp.tapir.docs.openapi._
    import sttp.tapir.openapi.circe.yaml._

    // interpreting the endpoint description to generate yaml openapi documentation
    val docs =
      List(
        Products.getProductsList,
        Products.getProductEndpoint,
        Products.postProductEndpoint,
        Products.getVariantEndpoint,
        Products.postVariantEndpoint,
        Products.putVariantEndpoint,
        Webhooks.getWebhooksEndpoint,
        Webhooks.postWebhooksEndpoint,
        Webhooks.deleteWebhooksEndpoints,
        getTaskEndpoint,
        getLayoutEndpoint,
        Catalog.getProductListEndpoint,
        Catalog.getProductEndpoint,
        Catalog.getProductVariantEndpoint,
        getShippingRatesEndpoint,
        order.postOrderEndpoint,
        order.getOrderEndpoint,
        order.getOrderListEndpoint,
        getTaxEndpoint,
        order.estimateOrderEndpoint
      ).toOpenAPI("Printful Api", "1.0")
        .servers(
          List(
            Server(s"http://localhost:$port", None),
            Server("http://localhost:9000", None),
            Server("http://api.printful.com", None)
          )
        )
    docs.toYaml
  }

  def printfulRoutes: Route = {
    import akka.http.scaladsl.server.Directives._
    import sttp.client._
    import sttp.client.logging.slf4j._
    import sttp.tapir.client.sttp._
    import sttp.tapir.server.akkahttp._

    import scala.concurrent.ExecutionContext.Implicits.global
    import scala.concurrent.Future

    implicit val backend: SttpBackend[Identity, Nothing, NothingT] =
      Slf4jCurlBackend[Identity, Nothing, NothingT](HttpURLConnectionBackend())
    val baseUri = uri"http://api.printful.com"

    def printful[I, O](endpoint: Endpoint[I, String, O, Nothing]) =
      httpProxy(endpoint, baseUri)

    def httpProxy[I, O](endpoint: Endpoint[I, String, O, Nothing], uri: Uri) = {
      val req: I => Request[Either[String, O], Nothing] = endpoint.toSttpRequestUnsafe(uri)

      endpoint.toRoute(i => Future { req(i).send().body })
    }

    val graphiql =
      path("graphiql.html") {
        getFromResource("graphiql.html")
      }
    // interpreting the endpoint description and converting it to an akka-http route, providing the logic which
    // should be run when the endpoint is invoked.
    printful(Products.getProductEndpoint) ~
      printful(Products.getProductsList) ~
      printful(Products.postProductEndpoint) ~
      printful(Products.getVariantEndpoint) ~
      printful(Webhooks.getWebhooksEndpoint) ~
      printful(Webhooks.postWebhooksEndpoint) ~
      printful(Webhooks.deleteWebhooksEndpoints) ~
      printful(getTaskEndpoint) ~
      printful(getLayoutEndpoint) ~
      printful(Catalog.getProductListEndpoint) ~
      printful(Catalog.getProductEndpoint) ~
      printful(Catalog.getProductVariantEndpoint) ~
      printful(getShippingRatesEndpoint) ~
      printful(getTaxEndpoint) ~
      printful(order.postOrderEndpoint) ~
      printful(order.getOrderEndpoint) ~
      printful(order.getOrderListEndpoint) ~
      printful(order.estimateOrderEndpoint) ~
      graphiql ~
      path("api" / "graphql") {
        api.routes
      }
  }

//  implicit class ElmPipeSyntax[A](val a: A) extends AnyVal {
//    def |>[B](fn: A => B): B = fn(a)
//  }

  def startServer(route: Route, yaml: String): Unit = {
    import akka.actor.ActorSystem
    import akka.http.scaladsl.Http
    import akka.http.scaladsl.server.Directives._

    import scala.concurrent.Await
    import scala.concurrent.duration._

    val routes = route ~ new SwaggerAkka(yaml).routes
    implicit val actorSystem: ActorSystem = ActorSystem()
    Await.result(Http().bindAndHandle(routes.log("logger"), "localhost", port), 1.minute)

    logger.info("Server started")
  }

  logger.info("Welcome to the Tapir Library example!")

  logger.info("Starting the server ...")
  startServer(printfulRoutes, openapiYamlDocumentation)

  logger.info("Making a request to the listing endpoint ...")

  logger.info(
    s"Try out the API by opening the Swagger UI: http://localhost:$port/docs"
  )
}
