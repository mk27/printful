package com.printful

import caliban.GraphQL
import com.printful.endpoints.{order, Catalog, Products}

object api {
  import sttp.client._
  val baseUri = uri"http://api.printful.com"
  import graphql.proxy

  private val gql: GraphQL[Any] =
    proxy(Catalog.getProductListEndpoint, baseUri) |+| // conflict due to both of these use the same name i think
      proxy(Catalog.getProductEndpoint, baseUri) |+|
      proxy(Catalog.getProductVariantEndpoint, baseUri) |+|
      proxy(Products.getProductsList, baseUri) |+|
      proxy(Products.getProductEndpoint, baseUri) |+|
      proxy(Products.getVariantEndpoint, baseUri) |+|
      proxy(order.getOrderEndpoint, baseUri) |+|
      proxy(order.getOrderListEndpoint, baseUri)

  val routes = graphql.toRoute(gql)
}
