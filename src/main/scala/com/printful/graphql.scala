package com.printful

import akka.http.scaladsl.server.Route
import caliban.interop.circe.CirceJsonBackend
import caliban.schema.ArgBuilder
import caliban.{AkkaHttpAdapter, GraphQL}
import sttp.client.asynchttpclient.WebSocketHandler
import sttp.client.logging.slf4j.Slf4jCurlBackend
import sttp.model.Uri
import sttp.tapir.Endpoint

object graphql {
  import caliban.interop.tapir._
  import sttp.client._
  import sttp.client.asynchttpclient.zio.AsyncHttpClientZioBackend
  import sttp.tapir.client.sttp._
  import zio._

  implicit val runtime = zio.Runtime.default
  import scala.concurrent.ExecutionContext.Implicits.global
  val adapter = AkkaHttpAdapter(new CirceJsonBackend)

  implicit val backend: SttpBackend[Task, Nothing, WebSocketHandler] =
    Slf4jCurlBackend[Task, Nothing, WebSocketHandler](Runtime.default.unsafeRun(AsyncHttpClientZioBackend()))

  def client[A, B](a: A, req: (A) => Request[Either[String, B], Nothing]): IO[String, B] = {
    backend.send(req(a)).bimap(_.getMessage, _.body).absolve
  }

  def proxy[I, O](endpoint: Endpoint[I, String, O, Nothing], uri: Uri)(
    implicit inputSchema: caliban.schema.Schema[Any, I],
    outputSchema: caliban.schema.Schema[Any, O],
    argBuilder: ArgBuilder[I]
  ): GraphQL[Any] = {
    val req: I => IO[String, O] = a => client(a, endpoint.toSttpRequestUnsafe(uri))
    endpoint.toGraphQL(req)
  }

  def toRoute(
    gql: GraphQL[Any]
  ): Route =
    adapter.makeHttpService(runtime.unsafeRun(gql.interpreter))
}
