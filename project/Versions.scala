object Versions {
  val circe = "0.13.0"
  val circeYaml = "0.13.1"
  val sttp = "2.1.4"
  val akkaHttp = "10.1.11"
  val tapir = "0.14.5"
  val akkaStreams = "2.6.5"
  val swaggerUi = "3.25.1"
  val scalaCheck = "1.14.3"
  val scalaTest = "3.0.8"
  val refined = "0.9.14"
  val enumeratum = "1.6.0"
  val caliban = "0.8.0"
}
